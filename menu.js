const request = require('request');

var getJson  = () => {
  return new Promise ( (resolve, reject) => {
    request({
      url: 'https://akfacterstirstaissighted:fd60d9408f6ec91bd7a2436f75b7c929be5bcc96@4234076e-aebf-453f-aa35-cc2c7f307d5d-bluemix.cloudant.com/botflores/menuprincipal'
    }, (error, response, body) => {
      if (error){
        reject(error);
      }
      else{
        resolve(JSON.parse(body));
      }
    });
  });
};

var getMessageData  = (jsonDoc) => {
  return new Promise( (resolve, reject) => {
    var messageData = {
      attachment: {
        type: "template",
        payload: {
        template_type: "generic",
          elements: []
        }
      }
    }
    //console.log(typeof(dtEspeciaisDosPais));

    jsonDoc.itemsMenu.forEach( (element, index, array) => {
      messageData.attachment.payload.elements[index] = {
        title: element.title,
        subtitle: element.itemDescription,
        image_url: element.image,
        buttons: [{
          type: "postback",
          title: "Escolher",
          payload: element.title
        }],
      }

    });

    resolve(messageData);
  });
};

var sendCard = (sender) => {

  getJson().then( (menuJSON) => {
    return getMessageData(menuJSON);
  }).then( (messageData) => {
    request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: process.env.FB_TOKEN },
        method: 'POST',
        json: {
          recipient: { id: sender },
          message: messageData
          // message:   messageDataOrig
        }
    } , function (error, response, body) {
          if (error) {
            console.log('Error sending message: ', error);
          } else if (response.body.error) {
              console.log('Error: ', response.body.error);
          }
    });
  }).catch( (errMsg) => {
    console.log(errMsg);
  });
};

module.exports.sendCard = sendCard;
