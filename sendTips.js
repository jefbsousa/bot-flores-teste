const request = require('request');


var getJson = (endpoint) => {
  return new Promise((resolve, reject) => {
    request({
      url: 'https://akfacterstirstaissighted:fd60d9408f6ec91bd7a2436f75b7c929be5bcc96@4234076e-aebf-453f-aa35-cc2c7f307d5d-bluemix.cloudant.com/botflores/' + endpoint
    }, (error, response, body) => {
      if (error) {
        reject('error get json');
      } else {
        resolve(JSON.parse(body));
      }
    });
  });
};

var getRandomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min) + min);
};

var sendImage = (sender, jsonDoc, random) => {
  return new Promise((resolve, reject) => {
    request({
      url: 'https://graph.facebook.com/v2.6/me/messages',
      qs: { access_token: process.env.FB_TOKEN },
      method: 'POST',
      json: {
          recipient: { id: sender },
          message: {
            attachment: {
              type: "image",
              payload: {
                url: jsonDoc.itens[random].image
                //jsonDoc.itens[2].image
              }
            }
          },
      }
    }, function (error, response, body) {
      if (error) {
        console.log('Error sending message: ', error);
        reject('error sendind message:');
      } else if (response.body.error) {
          console.log('Error: ', response.body.error);
          reject('error sendind message:');
      }
    });
  });
};
var sendText = (sender, jsonDoc, random) => {
  return new Promise((resolve, reject) => {
    request({
      url: 'https://graph.facebook.com/v2.6/me/messages',
      qs: { access_token: process.env.FB_TOKEN },
      method: 'POST',
      json: {
          recipient: { id: sender },
          message: {
              text: jsonDoc.itens[random].text
          }
      }
    }, function (error, response, body) {
      if (error) {
        console.log('Error sending message: ', error);
      } else if (response.body.error) {
          console.log('Error: ', response.body.error);
      }
    });
  });
};

var sendImageAndText = (sender, endpoint) => {
  return new Promise((resolve, reject) => {
    getJson(endpoint).then((tipsJSON) => {
      var random = getRandomNumber(0, 6);
      sendImage(sender, tipsJSON, random);
      sendText(sender, tipsJSON, random);
    });
  });
};

module.exports.sendImageAndText = sendImageAndText;
