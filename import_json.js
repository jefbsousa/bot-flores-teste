
var fs = require('fs');
var request = require('request');

// var dtEspeciaisDosPais = JSON.parse(fs.readFileSync('datasEspeciaisDiaDosPais.json'));
//
// //console.log(productsArr.products[1].itemName);
//
//
//
// var messageData = {
//   attachment: {
//     type: "template",
//     payload: {
//     template_type: "generic",
//       elements: [{
//         title: "First card",
//         subtitle: "Element #1 of an hscroll",
//         image_url: "https://s3-sa-east-1.amazonaws.com/avidotec/images/shoot-the-clown.jpg",
//         buttons: [{
//           type: "web_url",
//           url: "https://www.messenger.com",
//           title: "web url"
//         }, {
//           type: "postback",
//           title: "Postback",
//           payload: "Payload for first element in a generic bubble",
//         }],
//       }, ]
//     }
//   }
// }
//
// dtEspeciaisDosPais.products.forEach( (element, index, array) => {
//   //console.log(element.itemName);
//
//
//   messageData.attachment.payload.elements[index] = {
//     title: element.itemName,
//     subtitle: element.itemDescription,
//     image_url: element.itemURL,
//     buttons: [{
//       type: "web_url",
//       url: element.itemURL,
//       title: "link"
//     }, {
//       type: "postback",
//       title: "Postback",
//       payload: "Payload for first element in a generic bubble",
//     }],
//   }
//
// });
//
//
//
// fs.writeFileSync('add_cards.json', JSON.stringify(messageData), 'utf-8' );

//curl -XGET 'https://akfacterstirstaissighted:fd60d9408f6ec91bd7a2436f75b7c929be5bcc96@4234076e-aebf-453f-aa35-cc2c7f307d5d-bluemix.cloudant.com/botflores/datasEspeciais_diaDosPais'


// using nested callbacks
/*
var obj = {};

request({
    url: 'https://akfacterstirstaissighted:fd60d9408f6ec91bd7a2436f75b7c929be5bcc96@4234076e-aebf-453f-aa35-cc2c7f307d5d-bluemix.cloudant.com/botflores/datasEspeciais_diaDosPais'
  } ,  (error, response, body) => {
        if (error) {
          console.log('Error: ', error);
        } else {
            //console.log('body: ', body);
            obj = body;
            console.log("Got it! - body");
            //fs.writeFileSync('add_cards.json', obj, 'utf-8' );
            request({
              url: 'https://maps.googleapis.com/maps/api/geocode/json?address=Avenida+paulista+SP'
            }, (error, response, body) => {
              if (error){
                console.log(error);
              }
              else {
                console.log('Got it! - maps')
              }
            });
        }
});
*/

//using Promises

const diaDosPais = require('./import_json2');

diaDosPais.getJson().then( (json) => {
  console.log('get json');
  return diaDosPais.getMessageData(json);
}).then( (messageData) => {
  console.log('got messageData')
  //fs.writeFileSync('add_cards.json', JSON.stringify(messageData), 'utf-8' );
  return diaDosPais.getLatLong('https://maps.googleapis.com/maps/api/geocode/json?address=Avenida+paulista+SP');
}).then( () => {
  console.log('Get lat long');
}).catch( (errMsg) => {
  console.log(errMsg);
});







//
