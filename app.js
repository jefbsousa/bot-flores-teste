
var express = require('express');
var fs = require('fs');
var request = require('request');
var bodyParser = require('body-parser');
var Conversation = require('watson-developer-cloud/conversation/v1'); // watson sdk
var app = express();


require('dotenv').config({silent: true});

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var contexto_atual = null;

var w_conversation = new Conversation({
  url: 'https://gateway.watsonplatform.net/conversation/api',
  version_date: '2017-04-21',
  username: process.env.CONVERSATION_USERNAME,
  password: process.env.CONVERSATION_PASSWORD,
  version: 'v1'
});

app.get('/webhook/', function (req, res) {
	if (req.query['hub.verify_token'] === process.env.FB_TOKENVERIFIC) res.send(req.query['hub.challenge']);
	res.send('Erro de validação no token.');
});

app.post('/webhook/', function (req, res) {
  var text = null;

  messaging_events = req.body.entry[0].messaging;
	for (i = 0; i < messaging_events.length; i++) {
		event = req.body.entry[0].messaging[i];
    sender = event.sender.id;

    if (event.message && event.message.text) text = event.message.text;
    else if (event.postback ) text = event.postback.payload;


		//else if (event.postback && !text) text = event.postback.payload;
		else break;

	 var payload = {
  		workspace_id: process.env.WORKSPACE_ID,
  		context: contexto_atual || {},
  		input: { "text": text },
		  alternate_intents: true
		};

    callWatson(payload, sender);
  }


  res.sendStatus(200);
});

function callWatson(payload, sender) {
	w_conversation.message(payload, function (err, results) {
    	if (err) return responseToRequest.send("Erro > " + JSON.stringify(err));

		if(results.context != null) contexto_atual = results.context;

        if(results != null && results.output != null){
			var i = 0;
			while(i < results.output.text.length){
				sendMessage(sender, results.output.text[i++]);
			}
		}

    });
}

/***********************************************************************************************************
************************************************************************************************************/

function sendMessage(sender, text_) {
  // text_ = text_.substring(0, 319);
  const cardMaker = require('./cardMaker');
  const menu = require('./menu');
  const tips = require('./sendTips');

  switch(text_){

    case 'menu':
      menu.sendCard(sender);
      break;

/******************SubMenus *****************************/
    case 'ocasioesDatasEspeciais':
      cardMaker.sendCard(sender, 'submenu_OcasioesEDatasEspeciais', 'itemsMenu');
      break;

    case 'flores':
      cardMaker.sendCard(sender, 'submenu_Flores', 'itemsMenu');
      break;

    case 'especiais':
      cardMaker.sendCard(sender, 'submenu_Especiais', 'itemsMenu');
      break;

    case 'categorias':
      cardMaker.sendCard(sender, 'submenu_Categorias', 'itemsMenu');
      break;

    case 'dicas':
      tips.sendImageAndText(sender, 'dicasConservacao');
      break;

    case 'atendimento':
      cardMaker.sendCard(sender, 'submenu_AtendimentoAoCliente', 'itemsMenu');
      break;


/****************** categorias(SubMenu) *****************************/
    case 'bebidas':
      cardMaker.sendCard(sender, 'categorias_Bebidas', 'products');
      break;

    case 'beleza':
      cardMaker.sendCard(sender, 'categorias_Beleza', 'products');
      break;

    case 'caixasPresente':
      cardMaker.sendCard(sender, 'categorias_CaixasDePresente', 'products');
      break;

    case 'cestas':
      cardMaker.sendCard(sender, 'categorias_Cestas', 'products');
      break;

    case 'chocolate':
      cardMaker.sendCard(sender, 'categorias_Chocolate', 'products');
      break;

/****************** Datas Especiais *****************************/
    case 'amizades':
      cardMaker.sendCard(sender, 'datasEspeciais_Amizades', 'products');
      break;

    case 'novoBebe':
      cardMaker.sendCard(sender, 'datasEspeciais_NovoBebe', 'products');
      break;

    case 'parabens':
      cardMaker.sendCard(sender, 'datasEspeciais_Parabens', 'products');
      break;

    case 'romantico':
      cardMaker.sendCard(sender, 'datasEspeciais_Romantico', 'products');
      break;

    case 'veloriosFunerais':
      cardMaker.sendCard(sender, 'datasEspeciais_VeloriosEFunerais', 'products');
      break;

    case 'diaDosPais':
      cardMaker.sendCard(sender, 'datasEspeciais_diaDosPais', 'products');
      break;

/************************* Especiais *****************************/
    case 'carolCosta':
      cardMaker.sendCard(sender, 'especiais_CarolCosta', 'products');
      break;

    case 'Hoegaarden':
      cardMaker.sendCard(sender, 'especiais_Hoegaarden', 'products');
      break;

    case 'juanaMartinez':
      cardMaker.sendCard(sender, 'especiais_JuanaMartinez', 'products');
      break;

    case 'lifeFlowers':
      cardMaker.sendCard(sender, 'especiais_LifeFlowers', 'products');
      break;

    case 'ligiaKogos':
      cardMaker.sendCard(sender, 'especiais_LigiaKogos', 'products');
      break;

    case 'versailles':
      cardMaker.sendCard(sender, 'especiais_Versailles', 'products');
      break;

/************************* Flores *******************************/

    case 'buqueFlores':
      cardMaker.sendCard(sender, 'flores_BuqueDeFlores', 'products');
      break;

    case 'floresCampo':
      cardMaker.sendCard(sender, 'flores_FloresDoCampo', 'products');
      break;

    case 'mixFlores':
      cardMaker.sendCard(sender, 'flores_MixDeFlores', 'products');
      break;

    case 'orquideas':
      cardMaker.sendCard(sender, 'flores_Orquideas', 'products');
      break;

    case 'plantasESuculentas':
      cardMaker.sendCard(sender, 'flores_PlantasESuculentas', 'products');
      break;

    case 'sazonais':
      cardMaker.sendCard(sender, 'flores_Sazonais', 'products');
      break;


/****************** Atendimento Ao Cliente *****************************/

    case 'atendimento_entregas':
      cardMaker.sendCard(sender, 'submenu_Atendimento_Entregas', 'itemsMenu');
      break;

    case 'atendimento_compras':
      cardMaker.sendCard(sender, 'submenu_Atendimento_Compras', 'itemsMenu');
      break;

    case 'atendimento_produtos':
      cardMaker.sendCard(sender, 'submenu_Atendimento_Produtos', 'itemsMenu');
      break;

    case 'atendimento_seguranca':
      cardMaker.sendCard(sender, 'submenu_Atendimento_SegurancaEPrivacidade', 'itemsMenu');
      break;

    case 'atendimento_ouvidoria':
      cardMaker.sendCard(sender, 'submenu_Atendimento_Ouvidoria', 'itemsMenu');
      break;


/****************** Text responses generate in WATSON conversation (no cards) ************************/

    default:
      messageData = {text : text_} ;
      request({
        url: 'https://graph.facebook.com/v2.6/me/messages',
        qs: { access_token: process.env.FB_TOKEN },
        method: 'POST',
        json: {
            recipient: { id: sender },
            message: messageData,
        }
      }, function (error, response, body) {
        if (error) {
        	console.log('Error sending message: ', error);
        } else if (response.body.error) {
            console.log('Error: ', response.body.error);
        }
      });

      break;

  } //switch case

} //function

app.listen(process.env.PORT || 3000);



//
