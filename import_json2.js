const request = require('request');

// const urlDtEspeciaisDosPais = 'https://akfacterstirstaissighted:fd60d9408f6ec91bd7a2436f75b7c929be5bcc96@4234076e-aebf-453f-aa35-cc2c7f307d5d-bluemix.cloudant.com/botflores/datasEspeciais_diaDosPais';

var getJson  = () => {
  return new Promise ( (resolve, reject) => {
    request({
      url: 'https://akfacterstirstaissighted:fd60d9408f6ec91bd7a2436f75b7c929be5bcc96@4234076e-aebf-453f-aa35-cc2c7f307d5d-bluemix.cloudant.com/botflores/datasEspeciais_diaDosPais'
    }, (error, response, body) => {
      if (error){
        console.log('getJ-error');
        reject(error);
      }
      else{
        resolve(JSON.parse(body));
      }
    });
  });
};


var getMessageData  = (dtEspeciaisDosPais) => {
  return new Promise( (resolve, reject) => {
    var messageData = {
      attachment: {
        type: "template",
        payload: {
        template_type: "generic",
          elements: []
        }
      }
    }
    //console.log(typeof(dtEspeciaisDosPais));

    dtEspeciaisDosPais.products.forEach( (element, index, array) => {
      messageData.attachment.payload.elements[index] = {
        title: element.itemName,
        subtitle: element.itemDescription,
        image_url: element.itemPhoto,
        buttons: [{
          type: "web_url",
          url: element.itemURL,
          title: "link"
        }, {
          type: "postback",
          title: "Postback",
          payload: "Payload for first element in a generic bubble",
        }],
      }

    });

    resolve(messageData);
  });
};


var getLatLong = (urlJson) => {
  return new Promise ( (resolve, reject) => {
    request ({
      url: `${urlJson}`
    }, (error, response, body) => {
      if (error){
        console.log('latlong-error')
        reject(error);
      }
      else{
        resolve("Got it! - Maps");
      }
    });
  });
};


module.exports = {
  getJson ,
  getMessageData,
  getLatLong
};
